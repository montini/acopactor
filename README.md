## Software manual

This readme is meant to be an initial guidance through the software, describing its implementation and the recommended usage.

### About
The high-level goal of this library is to provide a framework built on the Scala implementation of Akka with actors able to dynamically tune their behaviour according to the Context, a set of knowledge accessed through both local and Aggregate sources.

By respecting the most common programming principles, this library offers a basic version with Context support only, and an enriched version that also enables the programmer to exploit the Aggregate advantages to compute the Context.

**Table of Contents**

[TOC]


## Context
The context package is the minimum working set of classes that can be used. It includes context orientation (everything regarding layers handling) and actors/messaging structures.

### ContextLayer
A layer is the lowest level entity of the entire system. When active, it provides its behaviour when reacting to incoming messages.

#### Implementation
Extending a layer requires the programmer to define its `priority`, `condition` and `behaviour`.

`val priority: Int` is represented with an Int number, which will be later used to sort the layers in ascending order. The order among layers with the same priority will be automatically set by Scala.

`def condition(): Boolean` is a boolean expression which will be evaluated by the software before and/or after the handling of a message, according to the message specifications (see [ContextActor](#contextactor))

`def behavior: PartialFunction[Any, Any]` is the Partial Function which will be called by the Actor itself with the latest received message. This method is to be intended as the Receive method of an actor able to handle only specific messages of the entire system, in its own way that takes into consideration the Context.

#### Features
Within the scope of a Layer, the programmer is also provided with two useful methods:
`reply(message: Any)` allows sending a custom message of any kind to the sender actor that triggered the entire execution.

`proceed(current: ContextLayer[S])` (used in the code as `proceed(this)`) lets the execution proceed through the following active layers for that specific received message, without re-evaluating the context.
**Note**: Not calling such method ad the end of the handler, blocks the execution. This can be useful if a layer with higher priority is meant to entirely substitute the behaviour of another one with lower priority.

### ContextState
The state is the shared knowledge between the layers. Each layer has the reference to it and the state has the list of each existing layer.

#### Implementation
The only requirement for the developer is to define a Set of existing layers, by overriding `layersPool`. This can be done by just writing a Set of case classes extending at some point the class `ContextLayer`. The library itself will be responsible of instantiating a class for each layer and setting the reference to the state.

#### Features
The state can be used as a bucket for every piece of information that has to be shared among each layer, as each layer has a reference to it.
Some methods are provided to get access to the most meaningful information:
 - The layers map (priority -> Set of layers).
 - A flat version of the layers set (sorted by priority, the same used when handling a message).
 - The last message received.
 - The sender of the last message.

### ContextActor
The actor extends from a regular Akka actor and is obviously responsible for the first handling of incoming messages. When created, it also initializes the state by creating the instances of each layer.

#### Implementation
There's nothing to extend from the ContextActor to make it work. Just instantiating one with the usual akka procedure is enough, with the only requirement to set the custom ContextState.

#### Features
The receive method stores sender and message on the state, then launches the computation of the underlying layers for that specific message. This last step is wrapped by two if statements, responsible of validating the traits of the upcoming message. If it extends by the traits `ReadBefore` or `ReadAfter` the state will be asked (before or after the handling, according to the trait names) to evaluate the context and renew the map of active layers.

The decision of whether a message requires an evaluation before the execution, after it or not at all is up to the programmer.

## Aggregate
The aggregate package is built on the context package, enriching it with aggregation features. Most notably the handling of neighbours and the basic structures to launch, process and close an aggregation (collection of pieces of information for the entire system).

Using the Aggregate- classes do not require additional mandatory implementation, as it just grants access to the set of methods and functionalities for free.

### AggregateLayer
#### Features
The only addition here is the `aggregate(id: String)` method. As of now, it requires a String ID (this can be easily expanded at a later point with a Generic type).

The method returns false if either there are no neighours or there is already an ongoing aggregation with that ID, and the timeout hasn't passed yet (still room for improvements, for instance by using `Either/Left/Right` as the return type to pass knowledge on why it may have failed).

On the success branch, the structures to handle the new aggregation process are instantiated and a `CollectMsg(id)` is sent to the neighbourhood.

### AggregateState
#### Features
The Aggregate version of the State contains all the aforementioned structures necessary to support an aggregation.

Most notably it contains the `neighbours`, as a `mutable.Set[ActorRef]`, updated from the outside with incoming messages handled by the main actor.

As a support to the programmer, this class also communicates to the outside the list of every pending aggregation (`pendingAggregations: Set[String]`) and a structure with the last executed ones (`lastExec: mutable.Map[String, Date]`). The main difference is that the latter has already received an answer by every neighbour, the first instead is still awaiting for everyone to answer, hence its result is not available yet.

### AggregateActor
#### Features
Main role of this extension is to capture aggregation-related messages, while passing every other message to the ContextActor above (super.receive). This is as simple as `override def receive: Receive =  aggregateReceive orElse super.receive`.

With the messages `[Add/Remove]NeighboursMsg(neighbours: ActorRef*)` it is possible to handle the neighbourhood from the outside. It works either with one or more ActorRef as parameter thanks to the varArg.

Incoming messages extending from `ACMessage` are handled separately. As of now only `CollectedMsg(id: String = "default", ans: Any)` falls here, but this can be later expanded to handle more specific aggregations.
Receiving a `CollectedMsg` stores the key-value information (sender -> (id, answer)) in the aggregation structures via `updateAggregation(id, sender(), ans)`, and sends a `CollectCompletedMsg` to `self` once it notices the message just received was the last one missing to complete the set of answers.