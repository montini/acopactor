package it.unibo.usage

import akka.actor.{ActorSystem, Props}
import it.unibo.lib.context.ContextActor
import it.unibo.usage.messages.BasicMessages._

object Main extends App {

  val system: ActorSystem = ActorSystem("System")

  val actor = system.actorOf(Props(ContextActor(new DemoActorState)), "TestActor")

  actor ! HelloMsg

}