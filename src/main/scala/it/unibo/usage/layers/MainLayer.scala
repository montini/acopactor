package it.unibo.usage.layers

import it.unibo.lib.messages.AC.{CollectCompletedMsg, CollectMsg, CollectedMsg}
import it.unibo.usage.DemoActorState
import it.unibo.usage.messages.BasicMessages._


case class MainLayer(override val state: DemoActorState) extends DemoLayer(state) {

  override val priority: Int = 5

  override def condition(): Boolean = true

  override val behavior: PartialFunction[Any, Any] = {
    case HelloMsg => reply("Hello"); proceed(this)
    case AddNumLayer => state.numLayerCondition = true; reply(true)
    case SumMsg(amount: Int) => state.num += amount; reply(state.num); proceed(this)
    case Add5Msg => reply(state.num)
    case RemoveNumLayer => state.numLayerCondition = false; reply(true)

    // Aggregate handlers
    case StartCollectingNum => aggregate("num")
    case CollectMsg(id) if id == "num" => reply(CollectedMsg(id, state.num))
    case CollectCompletedMsg(id, res) if id == "num" => state.num = res.values.map(_.asInstanceOf[Int]).sum
  }

  override def toString: String = "Actor Layer"

}
