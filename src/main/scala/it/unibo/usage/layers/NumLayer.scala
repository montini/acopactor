package it.unibo.usage.layers

import it.unibo.usage.DemoActorState
import it.unibo.usage.messages.BasicMessages.Add5Msg


case class NumLayer(override val state: DemoActorState) extends DemoLayer(state) {

  override val priority: Int = 1

  override def condition(): Boolean = state.numLayerCondition

  override val behavior: PartialFunction[Any, Any] = {
    case Add5Msg => state.num += 5; proceed(this)
  }

  override def toString: String = "Layer 2"

}
