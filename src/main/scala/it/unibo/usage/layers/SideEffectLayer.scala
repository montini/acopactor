package it.unibo.usage.layers

import it.unibo.usage.DemoActorState
import it.unibo.usage.messages.BasicMessages.SumMsg

case class SideEffectLayer(override val state: DemoActorState) extends DemoLayer(state) {

  override val priority: Int = 8

  override def condition(): Boolean = state.num > 10

  override val behavior: PartialFunction[Any, Any] = {
    case SumMsg(_) => state.num += 1; proceed(this)
  }

  override def toString: String = "Layer 3"

}