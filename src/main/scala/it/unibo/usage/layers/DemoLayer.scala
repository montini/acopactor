package it.unibo.usage.layers

import it.unibo.lib.aggregate.AggregateLayer
import it.unibo.usage.DemoActorState

abstract class DemoLayer(override val state: DemoActorState) extends AggregateLayer[DemoActorState](state)