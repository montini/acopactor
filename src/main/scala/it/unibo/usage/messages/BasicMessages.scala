package it.unibo.usage.messages

import it.unibo.lib.messages.ReadConditions.{ReadAfter, ReadBefore}

object BasicMessages {

  case object HelloMsg extends ReadBefore

  case object AddNumLayer extends ReadBefore with ReadAfter

  case object RemoveNumLayer extends ReadBefore with ReadAfter

  case object Add5Msg extends ReadBefore

  case class SumMsg(amount: Int) extends ReadBefore

  case object StartCollectingNum extends ReadBefore

}
