package it.unibo.usage

import it.unibo.lib.aggregate.AggregateState
import it.unibo.usage.layers.{MainLayer, NumLayer, SideEffectLayer}

class DemoActorState extends AggregateState[DemoActorState] {

  /**
    * The set of existing layers (by class) during at the launch of the program.
    */
  override val layersPool = Set(MainLayer, NumLayer, SideEffectLayer)

  /**
    * An Int used for demo purposes.
    */
  var num: Int = 0

  var numLayerCondition: Boolean = false

}
