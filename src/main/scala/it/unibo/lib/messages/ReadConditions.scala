package it.unibo.lib.messages

object ReadConditions {

  /**
    * Evaluates the context before executing the message handlers.
    */
  trait ReadBefore

  /**
    * Evaluates the context after executing the message handlers.
    */
  trait ReadAfter

}
