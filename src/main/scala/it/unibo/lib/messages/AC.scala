package it.unibo.lib.messages

import akka.actor.ActorRef
import it.unibo.lib.messages.ReadConditions.ReadAfter

object AC {

  sealed trait ACMessage

  /**
    * Used when replying to an aggregate request.
    *
    * @param id the ID of the aggregate request
    * @param ans the answer to the request
    */
  case class CollectedMsg(id: String = "default", ans: Any) extends ACMessage

  /**
    * Requesting an aggregate computation.
    *
    * @param id the ID of the aggregate request
    */
  case class CollectMsg(id: String = "default")

  /**
    * Message sent to itself by an actor when all other neighours have submitted their answer.
    *
    * @param id the ID of the aggregate request
    * @param res the collection of each answer
    */
  case class CollectCompletedMsg(id: String, res: Map[ActorRef, Any]) extends ReadAfter

}
