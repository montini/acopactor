package it.unibo.lib.messages

import akka.actor.ActorRef

object Neighbour {

  /**
    * Add neighbours to an actor.
    *
    * @param neighbours one or more ActorRefs
    */
  case class AddNeighboursMsg(neighbours: ActorRef*)

  /**
    * Remove neighbours to an actor.
    *
    * @param neighbours one or more ActorRefs
    */
  case class RemoveNeighboursMsg(neighbours: ActorRef*)

}
