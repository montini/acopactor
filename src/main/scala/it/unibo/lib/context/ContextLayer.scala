package it.unibo.lib.context

import scala.language.implicitConversions

abstract class ContextLayer[S <: ContextState[S]](val state: S) {

  /**
    * The priority of a layer
    */
  val priority: Int

  /**
    * The condition that must be true in order to activate the layer.
    *
    * @return true to activate the layer, false otherwise
    */
  def condition(): Boolean

  /**
    * The partial function that will be implemented by each layer to handle incoming messages
    */
  def behavior: PartialFunction[Any, Any]

  /**
    * Finds the index of the caller layer and continues the message propagation to the remaining layers.
    *
    * @param current current layer, the caller
    */
  def proceed(current: ContextLayer[S]): Unit = {
    // Removes all the already evaluated layers
    state.getFlatLayers.drop{
      // Finds the index of the caller, then adds 1 to proceed with the following layer
      state.getFlatLayers.indexOf(current) + 1
    } launchWith state.getLastMessage
  }

  /**
    * Sends a message to the actor who triggered the current execution.
    * @param message the message to be sent
    */
  def reply(message: Any): Unit = {
    state.getLastSender tell (message, state.self)
  }

}

object ContextLayer {

  /**
    * Case class used to add the launchWith method to a Seq thanks to the following implicit
    * @param layers the sequence of layers on the state
    * @tparam S the current state type
    */
  case class SeqWithLayerLauncher[S <: ContextState[S]](layers: Seq[ContextLayer[S]]) {
    def launchWith(message: Any): Unit = layers.find(_.behavior.isDefinedAt(message)).map(_.behavior(message))
  }

  implicit def toLaunchableSeq[S <: ContextState[S]](layerList: Seq[ContextLayer[S]]): SeqWithLayerLauncher[S] =
    SeqWithLayerLauncher(layerList)

}
