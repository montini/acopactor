package it.unibo.lib.context

import akka.actor.ActorRef

import scala.collection.mutable

abstract class ContextState[S <: ContextState[S]] {

  private val layers: mutable.SortedMap[Int, Set[ContextLayer[S]]] = mutable.SortedMap.empty
  private var lastSender: Option[ActorRef] = Option.empty
  private var lastMsg: Option[Any] = Option.empty

  /*
  * The set of existing layers (by class) during at the launch of the program.
  */
  private var lp: Set[_ <: ContextLayer[S]] = Set.empty

  /**
    * Reference to the actor owning this state.
    */
  var self: ActorRef = _

  /**
    * The set of existing layers (by class) during at the launch of the program. Populated by the user.
    */
  val layersPool: Set[S => ContextLayer[S]]

  /**
    * Converts the user friendly list in proper state entities.
    *
    * @param s the instantiated state.
    */
  private[context] def init(s: S): Unit = lp = layersPool.map(_(s))

  /**
    * Evaluates each layer condition to put the active ones on the layers map.
    */
  private[context] def readContext(): Unit = {
    layers.clear()
    lp.filter(_.condition()).foreach(l =>
      layers.put(l.priority, layers.getOrElse(l.priority, Set.empty[ContextLayer[S]]) + l)
    )
  }

  /**
    * Method to retrieve the layers map with priorities.
    *
    * @return layer map
    */
  def getLayers: mutable.Map[Int, Set[ContextLayer[S]]] = layers.clone()

  /**
    * Method to retrieve a list of layers based on their priority. Same priority layers may no be ordered.
    *
    * @return layer one-dimensional list
    */
  def getFlatLayers: Seq[ContextLayer[S]] = layers.values.flatMap(l => l.toSeq).toSeq

  private[lib] def storeMessage(message: Any): Unit = lastMsg = Some(message)
  def getLastMessage: Any = lastMsg.get

  private[lib] def storeSender(sender: ActorRef): Unit = lastSender = Some(sender)
  def getLastSender: ActorRef = lastSender.get

}
