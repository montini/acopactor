package it.unibo.lib.context

import akka.actor.Actor
import it.unibo.lib.messages.ReadConditions.{ReadAfter, ReadBefore}


case class ContextActor[S <: ContextState[S]](state: S) extends Actor {

  state.init(state)
  state.self = self

  override def receive: Receive = {
    case msg =>
      // Stores the last sender
      state.storeSender(sender())

      // Stores the received message
      state.storeMessage(msg)

      // Activates the layer by reading the context (evaluating layer condition method)
      if (msg.isInstanceOf[ReadBefore]) state.readContext()

      // Triggers the receive method of the first layer which is able to handle the received message with isDefinedAt(msg) in order
      // Each layer may call proceed() to trigger the following in the list with the same message
      state.getFlatLayers launchWith state.getLastMessage

      // Activates the layer by reading the context (evaluating layer condition method)
      if (msg.isInstanceOf[ReadAfter]) state.readContext()
  }

}
