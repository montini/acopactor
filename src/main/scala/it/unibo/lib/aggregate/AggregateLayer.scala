package it.unibo.lib.aggregate

import java.util.Date
import it.unibo.lib.context.ContextLayer
import it.unibo.lib.messages.AC.CollectMsg

abstract class AggregateLayer[S <: AggregateState[S]](override val state: S) extends ContextLayer[S](state) {

  /**
    * Starts an aggregation request.
    *
    * @param id the id of the aggregation
    */
  def aggregate(id: String): Boolean = {
    // Cannot start if no neighbours are registered
    if (state.neighbours.size == 1) return false

    // Check if timeout occurred.
    state.lastExec.find(_._1.equals(id)).foreach(e => {
      if (new Date().getTime - e._2.getTime < 120) return false
    })

    // Sets the date for now
    state.lastExec.put(id, new Date())

    // Creates a new map entry with the aggregate function ID and a new submap entry for the answers with its own value
    state.startAggregate(id, state.neighbours.size)

    // Launches the aggregate computation towards each neighbour
    state.neighbours.foreach(_.tell(CollectMsg(id), state.self))
    true
  }

}
