package it.unibo.lib.aggregate

import java.util.Date

import akka.actor.ActorRef
import it.unibo.lib.context.ContextState

import scala.collection.mutable

abstract class AggregateState[S <: AggregateState[S]] extends ContextState[S] {

  private val aggregateCollector: mutable.Map[String, (Int, Map[ActorRef, Any])] = mutable.Map.empty

  /**
    * The list of accessible actors on the system.
    */
  val neighbours: mutable.Set[ActorRef] = mutable.Set.empty

  /**
    * Structure to keep track of the last executed aggregate requests with their timestamp.
    */
  val lastExec: mutable.Map[String, Date] = mutable.Map.empty

  /**
    * Creates the structure to start an aggregation.
    *
    * @param id the id of the aggregation
    * @param size the amount of neighour at that time thar will receive the request
    */
  private[aggregate] def startAggregate(id: String, size: Int): Unit = aggregateCollector.put(id, (size, Map.empty))

  /**
    * Updates an aggregation upon receiving an answer.
    *
    * @param id the id of the aggregation
    * @param sender who provided its answer
    * @param answer the answer
    */
  private[aggregate] def updateAggregation(id: String, sender: ActorRef, answer: Any): Unit =
    aggregateCollector.put(id, (aggregateCollector(id)._1 - 1, aggregateCollector(id)._2 + (sender -> answer)))

  /**
    * Check whether an aggregation is complete (everyone answered) or not.
    *
    * @param id the id of the aggregation
    * @return true if complete, false otherwiser
    */
  private[aggregate] def aggregationComplete(id: String): Boolean = aggregateCollector(id)._1 == 0

  /**
    * Used to close an aggregation. It removes the structure.
    *
    * @param id the id of the aggregation
    * @return the map with all values and their senders
    */
  private[aggregate] def popAggregate(id: String): Map[ActorRef, Any] = {
    val result = aggregateCollector(id)._2
    aggregateCollector -= id
    result
  }

  /**
    * A Seq of every pending aggregation.
    *
    * @return sequence of each ID
    */
  def pendingAggregations: Set[String] = aggregateCollector.keys.toSet

}
