package it.unibo.lib.aggregate

import it.unibo.lib.context.ContextActor
import it.unibo.lib.messages.AC.{ACMessage, CollectCompletedMsg, CollectedMsg}
import it.unibo.lib.messages.Neighbour.{AddNeighboursMsg, RemoveNeighboursMsg}

class AggregateActor[S <: AggregateState[S]](state: S) extends ContextActor[S](state) {

  state.neighbours += self

  override def receive: Receive =  aggregateReceive orElse super.receive

  private def aggregateReceive: Receive = {
    case AddNeighboursMsg(neighbours @ _*) => state.neighbours ++= neighbours
    case RemoveNeighboursMsg(neighbours @ _*) => state.neighbours --= neighbours
    case msg: ACMessage => aggregateHandler(msg)
  }

  private def aggregateHandler: PartialFunction[ACMessage, Any] = {
    case CollectedMsg(id, ans) =>
      state.updateAggregation(id, sender(), ans)

      // if the aggregation is complete, it sends to itself a message with the final results
      if (state.aggregationComplete(id)) {
        self ! CollectCompletedMsg(id, state.popAggregate(id))
      }
  }

}
