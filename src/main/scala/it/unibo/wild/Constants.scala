package it.unibo.wild

import it.unibo.wild.interaction.location.{GeoRectangle, Point}

object Constants {

  /**
    * ID for the distance aggregation
    */
  val DISTANCE_ID: String = "Distance"

  /**
    * ID for the distance aggregation
    */
  val HUNGRY_ID: String = "Hungry"

  /**
    * Safe area, risk outside.
    */
  val safeArea: GeoRectangle = GeoRectangle(Point(0.0, 0.0), Point(1000.0, 1000.0))

  /**
    * Min distance to be considered alone.
    */
  val aloneThreshold: Double = 100

  /**
    * Max distance to be considered in a group.
    */
  val inGroupThreshold: Double = 15

}
