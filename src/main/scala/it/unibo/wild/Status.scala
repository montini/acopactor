package it.unibo.wild

class Status {

  //private val pattern: String = "uuuu/MM/dd HH:mm:ss:SSS"

  var name: String = ""
  //var time: Date = new Date
  var alone: Boolean = false
  var inGroup: Boolean = false
  var hungry: Boolean = false
  var hungryInRange: Int = 0
  var outside: Boolean = false


  def reset(actor: String): Unit = {
    //time = new Date
    name = actor
    alone = false
    inGroup = false
    hungry = false
    hungryInRange = 0
    outside = false
  }

  override def toString: String = {
    //s"[${time.formatted(pattern)} $name] - "
    val sb = new StringBuilder
    sb.append(s"[$name] ")
    if (alone) sb.append("Alone; ")
    if (inGroup) sb.append("Group; ")
    if (hungry) sb.append(s"Hungry ($hungryInRange); ")
    if (outside) sb.append("Outside; ")

    if (!alone && ! inGroup && !hungry && !outside) sb.append("OK")

    val str = sb.toString
    if (str.endsWith(" ")) str.dropRight(1) else str
  }
}
