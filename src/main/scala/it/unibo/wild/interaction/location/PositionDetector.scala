package it.unibo.wild.interaction.location

/**
  * Trait of a generic detector (could be GPS or mock) to define the position of an animal in the world.
  */
trait PositionDetector {

  var coord: Point

}
