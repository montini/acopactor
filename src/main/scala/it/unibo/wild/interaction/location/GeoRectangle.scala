package it.unibo.wild.interaction.location

case class GeoRectangle(bottomLeft: Point, topRight: Point) {

  def contains(point: Point): Boolean = {
    point.x >= bottomLeft.x && point.x <= topRight.x && point.y >= bottomLeft.y && point.y <= topRight.y
  }

}
