package it.unibo.wild.interaction.location

import scala.math.hypot

case class Point(x: Double, y: Double) {

  def distanceTo(other: Point): Double = {
    hypot(x - other.x, y - other.y)
  }

}