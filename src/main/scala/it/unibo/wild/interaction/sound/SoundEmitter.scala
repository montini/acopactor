package it.unibo.wild.interaction.sound

trait SoundEmitter {

  private var lastEmitted: Int = 0

  def emit(power: Int): Unit = lastEmitted = power

  def lastSoundEmitted: Int = lastEmitted

}
