package it.unibo.wild.messages

import it.unibo.lib.messages.ReadConditions.{ReadAfter, ReadBefore}

object BasicMessages {

  case object TickMsg extends ReadBefore with ReadAfter

  case object EmitSoundMsg extends ReadBefore

  case class UpdateMsg(status: String)

  case class PrintMsg(amount: Int)

}
