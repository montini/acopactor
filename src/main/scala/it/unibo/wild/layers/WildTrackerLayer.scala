package it.unibo.wild.layers

import it.unibo.lib.aggregate.AggregateLayer
import it.unibo.wild.WildTrackerActorState

abstract class WildTrackerLayer(override val state: WildTrackerActorState) extends AggregateLayer[WildTrackerActorState](state)