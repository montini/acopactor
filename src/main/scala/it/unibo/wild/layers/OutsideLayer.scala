package it.unibo.wild.layers

import it.unibo.wild.{Constants, WildTrackerActorState}
import it.unibo.wild.messages.BasicMessages.{EmitSoundMsg, TickMsg}

case class OutsideLayer(override val state: WildTrackerActorState) extends WildTrackerLayer(state) {

  override val priority: Int = 4

  override def condition(): Boolean = !Constants.safeArea.contains(state.position.coord)

  override val behavior: PartialFunction[Any, Any] = {
    case TickMsg =>
      state.status.outside = true
      proceed(this)
    case EmitSoundMsg => state.soundEmitter.emit(10)
  }

}
