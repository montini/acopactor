package it.unibo.wild.layers

import akka.actor.ActorRef
import it.unibo.lib.messages.AC.{CollectCompletedMsg, CollectMsg, CollectedMsg}
import it.unibo.wild.interaction.location.Point
import it.unibo.wild.messages.BasicMessages.TickMsg
import it.unibo.wild.{Constants, WildTrackerActorState}

case class AggregateReceiverLayer(override val state: WildTrackerActorState) extends WildTrackerLayer(state) {

  override val priority: Int = 1

  override def condition(): Boolean = true

  override val behavior: PartialFunction[Any, Any] = {
    case TickMsg =>
      aggregate(Constants.DISTANCE_ID)
      proceed(this)

    case CollectMsg(Constants.DISTANCE_ID) => reply(CollectedMsg(Constants.DISTANCE_ID, state.position.coord))
    case CollectCompletedMsg(Constants.DISTANCE_ID, res: Map[ActorRef, _]) =>
      state.minNeighbourDistance = Some(res.filterNot(_._1 equals state.self).values.map(_.asInstanceOf[Point].distanceTo(state.position.coord)).min)

    case CollectMsg(Constants.HUNGRY_ID) => reply(CollectedMsg(Constants.HUNGRY_ID, state.lastMeal))
    case CollectCompletedMsg(Constants.HUNGRY_ID, res: Map[ActorRef, _]) =>
      state.hungryNeighbours = res.keys.map(_.path.name).toSet
  }

}
