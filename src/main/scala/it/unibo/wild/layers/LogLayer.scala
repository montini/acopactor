package it.unibo.wild.layers

import it.unibo.wild.WildTrackerActorState
import it.unibo.wild.messages.BasicMessages.{TickMsg, UpdateMsg}

case class LogLayer(override val state: WildTrackerActorState) extends WildTrackerLayer(state) {

  override val priority: Int = 9

  override def condition(): Boolean = true

  override val behavior: PartialFunction[Any, Any] = {
    case TickMsg =>
      state.storageRef.get ! UpdateMsg(state.status.toString)

      state.status.reset(state.self.path.name)
  }

}
