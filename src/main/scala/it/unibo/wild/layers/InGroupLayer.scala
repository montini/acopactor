package it.unibo.wild.layers

import it.unibo.wild.messages.BasicMessages.TickMsg
import it.unibo.wild.{Constants, WildTrackerActorState}

case class InGroupLayer(override val state: WildTrackerActorState) extends WildTrackerLayer(state) {

  override val priority: Int = 4

  override def condition(): Boolean = state.minNeighbourDistance.fold(false)(_ < Constants.inGroupThreshold)

  override val behavior: PartialFunction[Any, Any] = {
    case TickMsg =>
      state.status.inGroup = true
      aggregate(Constants.HUNGRY_ID)
      proceed(this)
  }

}