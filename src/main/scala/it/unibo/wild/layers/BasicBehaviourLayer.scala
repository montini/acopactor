package it.unibo.wild.layers

import it.unibo.wild.WildTrackerActorState
import it.unibo.wild.messages.BasicMessages.EmitSoundMsg

case class BasicBehaviourLayer(override val state: WildTrackerActorState) extends WildTrackerLayer(state) {

  override val priority: Int = 10

  override def condition(): Boolean = true

  override val behavior: PartialFunction[Any, Any] = {
    case EmitSoundMsg => state.soundEmitter.emit(3)
  }
}
