package it.unibo.wild.mock

import it.unibo.wild.interaction.location.{Point, PositionDetector}

/**
  * Not a real detector but rather a dummy class that needs to get its values modified from the outside.
  *
  * Mainly for testing and demo purposes.
  */
class DummyPositionDetector extends PositionDetector {

  override var coord: Point = Point(0,0)

}
