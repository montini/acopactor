package it.unibo.wild.mock

import it.unibo.wild.interaction.sound.SoundEmitter

class DummySoundEmitter extends SoundEmitter {

  override def emit(power: Int): Unit = {
    super.emit(power)
    println(s"Sound emitted at ${power}Db!")
  }

}
