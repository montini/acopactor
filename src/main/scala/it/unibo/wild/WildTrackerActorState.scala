package it.unibo.wild

import java.util.Date

import akka.actor.ActorRef
import it.unibo.lib.aggregate.AggregateState
import it.unibo.wild.interaction.location.PositionDetector
import it.unibo.wild.interaction.sound.SoundEmitter
import it.unibo.wild.layers._
import it.unibo.wild.mock.{DummyPositionDetector, DummySoundEmitter}

class WildTrackerActorState(s: Option[ActorRef] = None, p: PositionDetector = new DummyPositionDetector, se: SoundEmitter = new DummySoundEmitter) extends AggregateState[WildTrackerActorState] {

  override val layersPool = Set(LogLayer, AggregateReceiverLayer, AloneLayer, OutsideLayer, AggregateReceiverLayer, InGroupLayer, BasicBehaviourLayer)

  val storageRef: Option[ActorRef] = s

  val position: PositionDetector = p
  var minNeighbourDistance: Option[Double] = None

  val soundEmitter: SoundEmitter = se

  var lastMeal: Option[Date] = None
  var hungryNeighbours: Set[String] = Set.empty

  val status: Status = new Status

}
