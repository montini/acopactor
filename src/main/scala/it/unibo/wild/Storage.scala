package it.unibo.wild

import akka.actor.Actor
import it.unibo.wild.messages.BasicMessages.{PrintMsg, UpdateMsg}

import scala.collection.mutable

class Storage extends Actor {

  var log: mutable.Buffer[String] = mutable.Buffer.empty

  override def receive: Receive = {
    case UpdateMsg(s: String) => log.+=:(s)
    case PrintMsg(amount) => sender ! log.take(amount)
  }

}
