package it.unibo.usage.uti

import it.unibo.lib.aggregate.AggregateActor
import it.unibo.usage.DemoActorState

class TestActor(state: DemoActorState = new DemoActorState) extends AggregateActor[DemoActorState](state) {

  override def receive: Receive =  testReceive orElse super.receive

  private val testReceive: Receive = {
    case "numVal" => sender() ! state.num
    case "Neighbours" => sender() ! state.neighbours
  }

}
