package it.unibo.usage

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import it.unibo.lib.messages.Neighbour.{AddNeighboursMsg, RemoveNeighboursMsg}
import it.unibo.usage.layers.{DemoLayer, MainLayer, SideEffectLayer}
import it.unibo.usage.messages.BasicMessages.{StartCollectingNum, SumMsg}
import it.unibo.usage.uti.TestActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.collection.mutable


class AggregateSpecs extends TestKit(ActorSystem("AggregateSpecs"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val state = new DemoActorState
  val mainActor: ActorRef = system.actorOf(Props(new TestActor(state)), "mainActor")
  val responder1: ActorRef = system.actorOf(Props(new TestActor), "responder1")
  val responder2: ActorRef = system.actorOf(Props(new TestActor), "responder2")

  "An actor" must {
    "set his neighbours" in {
      mainActor ! AddNeighboursMsg(responder1, responder2)

      mainActor ! "Neighbours"
      expectMsg(mutable.Set(mainActor, responder1, responder2))
    }

    responder1 ! SumMsg(15)
    expectMsg(15)

    responder2 ! SumMsg(3)
    expectMsg(3)

    mainActor ! SumMsg(4)
    expectMsg(4)

    "collect info via Collect" in {
      mainActor ! "numVal"
      expectMsg(4)

      assert(state.getFlatLayers.length == 1)

      mainActor ! StartCollectingNum

      // Give some time to the actor to send, receive and process the data
      Thread.sleep(120)

      mainActor ! "numVal"
      expectMsg(22)

      // SideEffectLayer has been applied as the state changed
      assert(state.getFlatLayers.length == 2)
      assert(state.getFlatLayers.exists(_.isInstanceOf[SideEffectLayer]))
    }

    "be able to remove a neighbour" in {
      // Testing one removal
      mainActor ! RemoveNeighboursMsg(responder1)
      mainActor ! "Neighbours"
      expectMsg(mutable.Set(mainActor, responder2))

      // Adding back the one removed
      mainActor ! AddNeighboursMsg(responder1)
      mainActor ! "Neighbours"
      expectMsg(mutable.Set(mainActor, responder1, responder2))

      // Testing varargs removal
      mainActor ! RemoveNeighboursMsg(responder1, responder2)
      mainActor ! "Neighbours"
      expectMsg(mutable.Set(mainActor))
    }
  }

}
