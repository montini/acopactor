package it.unibo.usage

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import it.unibo.usage.layers.{DemoLayer, MainLayer}
import it.unibo.usage.messages.BasicMessages._
import it.unibo.usage.uti.TestActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.collection.mutable

class DemoActorSpecs extends TestKit(ActorSystem("DemoActorSpecs"))
    with ImplicitSender
    with WordSpecLike
    with Matchers
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "My Cop Actor" must {

    val state = new DemoActorState
    val actor: ActorRef = system.actorOf(Props(new TestActor(state)), "TestActor")

    "reply to HelloMsg" in {
      actor ! HelloMsg
      expectMsg("Hello")
    }

    "make sure a a new layer is added for the next execution by changing the context" in {
      actor ! AddNumLayer
      expectMsg(true)
      Thread.sleep(100)

      assert(state.getFlatLayers.length == 2)
    }

    "remove a layer by changing the context" in {
      actor ! RemoveNumLayer
      expectMsg(true)
      Thread.sleep(100)

      // Only one layer is active now
      assert(state.getFlatLayers.length == 1)

      // The only active layer has to be the expected one, MainLayer at priority 5
      assert(state.getLayers(5).forall(_.isInstanceOf[MainLayer]))
    }

    "add 5 only if the AddNumLayer layer is applied" in {
      // Num is not increased as MainLayer itself doesn't do maths but only reply back in this case
      actor ! Add5Msg
      expectMsg(0)

      actor ! AddNumLayer
      expectMsg(true)

      // Num is now increased by 5
      actor ! Add5Msg
      expectMsg(5)

      actor ! Add5Msg
      expectMsg(10)

      actor ! RemoveNumLayer
      expectMsg(true)

      // Num no longer changes as NumLayer has been removed
      actor ! Add5Msg
      expectMsg(10)
    }

    "respect the layers priority order" in {
      actor ! SumMsg(2)
      expectMsg(12)

      actor ! "numVal"
      expectMsg(12)

      // Now that num > 10 the layer SideEffect is active

      // SideEffectLayer adds 1 to the total, but only after NumLayer execution and MainLayer response
      actor ! SumMsg(2)
      expectMsg(14)

      // SideEffect has added 1, but it happened after NumLayer message so the real amount is 15
      actor ! "numVal"
      expectMsg(15)
    }

  }
}
