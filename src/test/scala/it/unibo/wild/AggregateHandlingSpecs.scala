package it.unibo.wild

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import it.unibo.lib.messages.Neighbour.AddNeighboursMsg
import it.unibo.wild.interaction.location.Point
import it.unibo.wild.messages.BasicMessages.{PrintMsg, TickMsg}
import it.unibo.wild.uti.TestMessages.{RepositionMsg, _}
import it.unibo.wild.uti.WildTrackerTestActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class AggregateHandlingSpecs extends TestKit(ActorSystem("AggregateHandlingSpecs"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val storage: ActorRef= system.actorOf(Props(new Storage), "Storage")

  val animal1: ActorRef = system.actorOf(Props(new WildTrackerTestActor(storage)), "Animal1")
  val animal2: ActorRef = system.actorOf(Props(new WildTrackerTestActor(storage)), "Animal2")
  val animal3: ActorRef = system.actorOf(Props(new WildTrackerTestActor(storage)), "Animal3")


  "The tracker on animal1" must {

    "have no min distance at the beginning" in {
      animal1 ! MinDistMsg
      expectMsg(None)
    }

    "detect the distance of the closest" in {
      animal2 ! RepositionMsg(Some(120), None)
      expectMsg(Point(120.0, 0))
      animal2 ! TickMsg

      animal3 ! RepositionMsg(Some(230), None)
      expectMsg(Point(230.0, 0))
      animal3 ! TickMsg

      animal1 ! TickMsg

      animal1 ! AddNeighboursMsg(animal2, animal3)

      animal1 ! TickMsg

      Thread.sleep(100)
      animal1 ! MinDistMsg
      expectMsg(Some(120.0))
    }

    "detect if it's alone" in {
      animal1 ! TickMsg

      Thread.sleep(100)
      storage ! PrintMsg(1)
      expectMsg(Seq("[Animal1] Alone;"))

    }

    "detect if an animal comes closed" in {
      animal2 ! MoveMsg(Some(-50), None)
      expectMsg(Point(70.0, 0))
      animal2 ! TickMsg

      animal1 ! TickMsg
      Thread.sleep(120)


      animal1 ! TickMsg
      Thread.sleep(120)

      animal1 ! MinDistMsg
      expectMsg(Some(70.0))

      storage ! PrintMsg(1)
      expectMsg(Seq("[Animal1] OK"))
    }

  }


}
