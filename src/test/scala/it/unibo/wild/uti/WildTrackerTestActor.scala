package it.unibo.wild.uti

import akka.actor.ActorRef
import it.unibo.lib.aggregate.AggregateActor
import it.unibo.wild.WildTrackerActorState
import it.unibo.wild.interaction.location.Point
import it.unibo.wild.uti.TestMessages._

class WildTrackerTestActor(storage: ActorRef) extends AggregateActor(new WildTrackerActorState(Some(storage))) {

  override def receive: Receive =  testReceive orElse super.receive

  private val testReceive: Receive = {
    case CoordMsg => sender ! state.position.coord
    case MinDistMsg => sender ! state.minNeighbourDistance
    case LastMealMsg => sender ! state.lastMeal
    case LastSoundMsg => sender ! state.soundEmitter.lastSoundEmitted
    case MoveMsg(x: Option[Double], y: Option[Double]) =>
      val oldX = state.position.coord.x
      val oldY = state.position.coord.y
      val newX = x.getOrElse(oldX)
      val newY = y.getOrElse(oldY)
      state.position.coord = Point(oldX + newX, oldY + newY)
      sender() ! state.position.coord
    case RepositionMsg(x: Option[Double], y: Option[Double]) =>
      val newX = x.getOrElse(state.position.coord.x)
      val newY = y.getOrElse(state.position.coord.y)
      state.position.coord = Point(newX, newY)
      sender() ! state.position.coord
    case SetMealMsg(t) => state.lastMeal = Some(t)
  }

}