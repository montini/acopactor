package it.unibo.wild.uti

import java.util.Date

object TestMessages {

  val CoordMsg = "coord"
  val MinDistMsg = "minDist"
  val LastMealMsg = "lastMeal"
  val LastSoundMsg = "lastSoundMsg"

  case class SetMealMsg(time: Date)
  case class MoveMsg(x: Option[Double], y: Option[Double])
  case class RepositionMsg(x: Option[Double], y: Option[Double])

}

