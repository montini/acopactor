package it.unibo.wild

import java.util.Date

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import it.unibo.wild.interaction.location.Point
import it.unibo.wild.messages.BasicMessages.{PrintMsg, TickMsg}
import it.unibo.wild.uti.TestMessages._
import it.unibo.wild.uti.WildTrackerTestActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class WildTrackerBasicSpecs extends TestKit(ActorSystem("AnimalBasicsSpecs"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val storage: ActorRef = system.actorOf(Props(new Storage), "Storage")
  val tracker: ActorRef = system.actorOf(Props(new WildTrackerTestActor(storage)), "Tracker")

  "A basic system" must {

    "have its default values" in {
      tracker ! CoordMsg
      expectMsg(Point(0.0, 0.0))

      tracker ! MinDistMsg
      expectMsg(None)

      tracker ! LastMealMsg
      expectMsg(None)
    }

    "correctly set dummy position values" in {
      tracker ! RepositionMsg(Some(3.0), None)
      expectMsg(Point(3.0, 0.0))

      tracker ! RepositionMsg(None, None)
      expectMsg(Point(3.0, 0.0))

      tracker ! MoveMsg(Some(1.0), Some(3.2))
      expectMsg(Point(4.0, 3.2))
    }

    "correctly set dummy last meal info" in {
      val time = new Date()
      tracker ! SetMealMsg(time)
      tracker ! LastMealMsg
      expectMsg(Some(time))
    }

    "send logs to the storage" in {
      tracker ! TickMsg
      tracker ! TickMsg

      Thread.sleep(50)

      storage ! PrintMsg(2)
      expectMsg(Seq("[Tracker] OK", "[] OK"))
    }


  }



}
