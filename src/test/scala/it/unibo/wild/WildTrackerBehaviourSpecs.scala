package it.unibo.wild

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import it.unibo.wild.interaction.location.Point
import it.unibo.wild.messages.BasicMessages.{EmitSoundMsg, PrintMsg, TickMsg}
import it.unibo.wild.uti.TestMessages.{MoveMsg, _}
import it.unibo.wild.uti.WildTrackerTestActor
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class WildTrackerBehaviourSpecs extends TestKit(ActorSystem("AnimalBasicsSpecs"))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val storage: ActorRef = system.actorOf(Props(new Storage), "Storage")
  val tracker: ActorRef = system.actorOf(Props(new WildTrackerTestActor(storage)), "Tracker")

  "A tracker " must {
    "detect if the animal is outside the safe area" in {
      tracker ! TickMsg
      Thread.sleep(50)

      storage ! PrintMsg(1)
      expectMsg(Seq("[] OK"))

      tracker ! MoveMsg(Some(-2.0), None)
      expectMsg(Point(-2.0, 0.0))

      tracker ! TickMsg
      Thread.sleep(50)

      storage ! PrintMsg(1)
      expectMsg(Seq("[Tracker] Outside;"))
    }

    "detect when it comes back inside the safe area" in {
      tracker ! MoveMsg(Some(7.0), None)
      expectMsg(Point(5.0, 0.0))

      tracker ! TickMsg
      Thread.sleep(50)

      storage ! PrintMsg(3)
      expectMsg(Seq("[Tracker] OK", "[Tracker] Outside;", "[] OK"))
    }

    "emit a proper sound level while inside the safe area" in {
      tracker ! EmitSoundMsg
      tracker ! LastSoundMsg
      expectMsg(3)


      tracker ! RepositionMsg(None, Some(-15.0))
      expectMsg(Point(5.0, -15.0))

      tracker ! EmitSoundMsg
      tracker ! LastSoundMsg
      expectMsg(10)
    }
  }

}
